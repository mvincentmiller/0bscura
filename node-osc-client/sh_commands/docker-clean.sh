
#!/bin/bash

# 'docker system info'
# docker system info

# start docker machine
# docker-machine start default

# load environment
# eval $(docker-machine env)

# show system events
# docker system events

# show all containers, images
# docker images list -a
# docker ps -a

# show disk usage
docker system df

# stop all containers
docker stop $(docker ps -q)

# remove all unused images and volumes
docker system prune -a
