const osc = require('node-osc');
const io = require('socket.io')(8081);
const finalhandler = require('finalhandler')
const http = require('http')
const serveStatic = require('serve-static')

let serve = serveStatic('public', {'index': ['index.html']})

let server = http.createServer(function onRequest (req, res) {
  serve(req, res, finalhandler(req, res))
})

var oscServer = new osc.Server(5004, '0.0.0.0');
oscServer.on("message", function (msg, rinfo) {
      console.log(msg);
});

io.sockets.on('connection', function (socket) {
	console.log('connection');
  oscServer.on('message', function(msg, rinfo) {
    console.log(msg, rinfo.address);
    socket.emit("message", msg);
  });
});



console.log('static server on 3000')
server.listen(3000)
