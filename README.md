# Stuff For Charles

## Google Foobar

This is my submission to the Google Foobar recruiting challenge, which I received from my search profile (I was researching 802.11 and RSA encryption at the time).

Please note the comments (I was able to complete most, but not all challenges) and the testing harness I used to test and time my answers.

## Python, Node.js and Open Sound Control
This is a proof of concept I've been meaning to get around to which seemed appropriate. I did it this weekend.

### node-osc-client

This folder contains the Dockerfile and source for a service currently running on Amazon EC2 at

``` http://54.200.142.46:3000/ ``` You should see a black screen until someone (you) runs the service below.

Please note the 'sh_commands' folder.

### python-osc-server

This is the companion dummy OSC service which sends random values to the above mentioned EC2 instance over UDP.
The OSC protocol rides on UDP, and then websockets to the client.  

``` docker-compose up --build ``` will make a pretty picture happen at ``` http://54.200.142.46:3000/ ```

## hustleco

This was a pet project for a possible submission to Techstars Music earlier this year. I thought it might also be good to include some Flask development, who knows, internal tools and what not. I do have more web application stuff to show, but that isn't our focus. This app is running at

``` hustleco.herokuapp.com ```

and connects to a Postgres DB which is fairly automagically connected to Heroku: SQLAlchemy just needed the proper configuration. I was using SQLite locally.
