"""Stairs."""


def answer(n):
    """Bottom up."""
    m = [[0 for i in range(n + 1)] for j in range(n + 1)]
    m[0][0] = 1  # base case

    for last in range(1, n + 1):
        for left in range(0, n + 1):
            m[last][left] = m[last - 1][left]
            if left >= last:
                m[last][left] += m[last - 1][left - last]
    return m[n][n] - 1


def count(height, left):
    """Top Down."""
    global mem
    if mem[height][left] != -1:
        return mem[height][left]
    # all the bricks have been used
    if left == 0:
        return 1
    # not enough bricks to build a new stair
    if left < height:
        return 0

    # either build a new stair now or try the next height (height + 1)
    r = count(height + 1, left - height) + count(height + 1, left)
    mem[height][left] = r
    return r


def answer2(n):
    """Doc."""
    global mem
    # memoization
    mem = [[-1 for j in range(n + 2)] for i in range(n + 2)]
    return count(1, n) - 1
