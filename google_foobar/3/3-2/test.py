"""Doc."""
import timeit
from stairs import answer, answer2


def test(a, n):
    """Doc."""
    if (n == 3):
        start = timeit.default_timer()
        r = a(n)
        if r == 1:
            stop = timeit.default_timer()
            time = stop - start
            p = [True, r, time]
            return p
        else:
            stop = timeit.default_timer()
            time = stop - start
            p = [r, False, time]
            return p


def test2(a, n):
    """Doc."""
    if (n == 200):
        start = timeit.default_timer()
        r = a(n)
        if r == 487067745:
            stop = timeit.default_timer()
            time = stop - start
            p = [True, r, time]
            return p
        else:
            stop = timeit.default_timer()
            time = stop - start
            p = [r, False, time]
            return p


def test3(a, n):
    """Doc."""
    if (n == 5):
        start = timeit.default_timer()
        r = a(n)
        if r == 2:
            stop = timeit.default_timer()
            time = stop - start
            p = [True, r, time]
            return p
        else:
            stop = timeit.default_timer()
            time = stop - start
            p = [r, False, time]
            return p


def run_test():
    """Doc."""
    i = 0
    r = []
    while i <= 199:
        i = i + 1
        if answer(i) == answer2(i):
            r.append([True, i, answer2(i)])
        else:
            r.append([False, i, answer(i), answer2(i)])
            return r
    r.append(test(answer, 3))
    r.append(test2(answer, 200))
    r.append(test3(answer, 5))
    r.append(test(answer2, 3))
    r.append(test2(answer2, 200))
    r.append(test3(answer2, 5))
    return r


for r in run_test():
    print r
print(answer2(212))
