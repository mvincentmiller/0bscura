"""Tests for foobar 3.1."""

from checksum4 import answer, answer3
import timeit


def test(s, l, o):
    """Doc."""
    if (s == 17 and l == 4):
        start = timeit.default_timer()
        r = o(s, l)
        if r == 14:
            stop = timeit.default_timer()
            time = stop - start
            p = [r, True, time]
            return p
        else:
            stop = timeit.default_timer()
            time = stop - start
            p = [r, False, time]
            return p


def test_two(s, l, o):
    """Doc."""
    if (s == 0 and l == 3):
        start = timeit.default_timer()
        r = o(s, l)
        stop = timeit.default_timer()
        time = stop - start
        if o(s, l) == 2:
            p = [r, True, time]
            return p
        else:
            p = [r, False, time]
            return p


def test_three(s, l, o):
    """Doc."""
    if (s == 17 and l == 3):
        start = timeit.default_timer()
        r = o(s, l)
        stop = timeit.default_timer()
        time = stop - start
        if o(s, l) == 17 ^ 18 ^ 19 ^ 20 ^ 21 ^ 23:
            p = [r, True, time]
            return p
        else:
            p = [r, False, time]
            return p


def test_four(s, l, o):
    """Doc."""
    if (s == 2000 and l == 4):
        start = timeit.default_timer()
        r = o(s, l)
        stop = timeit.default_timer()
        time = stop - start
        xor_sum = 2000 ^ 2001 ^ 2002 ^ 2003 ^ 2004 ^ 2005 ^ 2006 ^ 2008 ^ 2009 ^ 2012
        if o(s, l) == xor_sum:
            p = [r, True, time]
            return p
        else:
            p = [r, False, time]
            return p


def test_five(s, l, o):
    """Doc."""
    start = timeit.default_timer()
    r = o(s, l)
    stop = timeit.default_timer()
    time = stop - start
    print round(time, 2)
    if time > 2.1:
        p = [r, False, time]
    else:
        p = [r, True, time]
    return p


def run_tests():
    """Doc."""
    # 14
    s1 = 17
    l1 = 4
    # 2
    s2 = 0
    l2 = 3
    # 6
    s3 = 17
    l3 = 3
    # 26
    s4 = 2000
    l4 = 4
    s5 = 2000000000
    l5 = 2122

    print(' \n')

    print('answer: ')
    print(test_five(s5, l5, answer))
    print(test(s1, l1, answer), test_two(s2, l2, answer))
    print(test_three(s3, l3, answer), test_four(s4, l4, answer))
    # print('answer 2: ')
    # print(test_five(s5, l5, answer2))
    # print(test(s1, l1, answer2), test_two(s2, l2, answer2))
    # print(test_three(s3, l3, answer2), test_four(s4, l4, answer2))
    print('answer 3: ')
    print(test_five(s5, l5, answer3))
    print(test(s1, l1, answer3), test_two(s2, l2, answer3))
    print(test_three(s3, l3, answer3), test_four(s4, l4, answer3))
    # print('answer 4: ')
    # print(test_five(s5, l5, answer4))
    # print(test(s1, l1, answer4), test_two(s2, l2, answer4))
    # print(test_three(s3, l3, answer4), test_four(s4, l4, answer4))


run_tests()
