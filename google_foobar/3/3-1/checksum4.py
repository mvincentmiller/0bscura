"""checksum4.py."""
import operator


# def f(x):
#     """Doc."""
#     return [x, 1, x + 1, 0][x % 4]


def xor(a, b):
    """Doc."""
    # return f(b) ^ f(a - 1)
    return [b, 1, b + 1, 0][b % 4] ^ [(a - 1), 1, (a - 1) + 1, 0][(a - 1) % 4]


def answer(start, length):
    """Doc."""
    l = length
    r = 0
    while l > 0:
        # r ^= xor(start, start + l - 1)
        a = start
        b = start + l - 1
        r ^= [b, 1, b + 1, 0][b % 4] ^ [(a - 1), 1, (a - 1) + 1, 0][(a - 1) % 4]
        start = start + length
        l = l - 1
    return r


def xor_in_range(a, b):
    """Doc."""
    return reduce(lambda x, y: x ^ y, xrange(a, b), 0)


def answer2(s, l):
    """Doc."""
    return reduce(operator.xor, (xor_in_range(begin, begin+l-i) for i, begin in enumerate(xrange(s, s + l * l, l))), 0)


def answer3(start, length):
    """Doc."""
    checksum = 0
    try:
        for s in xrange(length, 0, -1):
            for x in xrange(start, start + s):
                checksum ^= x
            start += length
    except MemoryError:
        return None
    return checksum

# def answer3(start, length):
#     """Doc."""
#     checksum = None
#     i = 0
#     while length > 0:
#         sublength = length
#         while sublength > 0:
#             if checksum:
#                 checksum ^= start
#             else:
#                 checksum = start
#             start += 1
#             sublength -= 1
#         length -= 1
#         start += i
#         i += 1
#     return checksum


# def answer4(start, length):
#     """Doc."""
#     checksum = reduce(lambda x,y: x ^ y, xrange(start, start + length), 0)
#     i = 0
#     while length > 0:
#         start += length + i
#         length -= 1
#         i += 1
#         checksum ^= reduce(lambda x,y: x ^ y, xrange(start, start + length), 0)
#     return checksum
