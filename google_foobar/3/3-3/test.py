"""Tests for 3.2."""
import timeit
from answers import answer, answer2


def test(a, n):
    """Doc."""
    if n == 15:
        start = timeit.default_timer()
        r = a(n)
        stop = timeit.default_timer()
        time = stop - start
        print r
        if r == 5:
            return [r, True, time]
        else:
            return False


def test2(a, n):
    """Doc."""
    if n == 4:
        start = timeit.default_timer()
        r = a(n)
        print r
        stop = timeit.default_timer()
        time = stop - start
        if r == 2:
            return [r, True, time]
        else:
            return False


def run_test():
    """Doc."""
    i = 1
    while i < 309:
        i = i + 1
        if answer(i) != answer2(i):
            print 'Fail.'
    print "Answer: "        
    print test(answer, 15)
    print test2(answer, 4)
    print "Answer 2: "
    print test(answer2, 15)
    print test2(answer2, 4)


run_test()
