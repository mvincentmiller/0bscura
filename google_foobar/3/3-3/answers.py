"""Doc."""


def answer(n):
    """Doc."""
    count = 0
    while n > 1:
        if n % 2 == 0:             # bitmask: *0
            n = n // 2
        elif n == 3 or n % 4 == 1:  # bitmask: 01
            n = n - 1
        else:                      # bitmask: 11
            n = n + 1
        count += 1
    return count


def answer2(n):
    """Doc."""
    count_to_1 = lambda x: bin(x)[:: -1].index('1')

    if n in [0, 1]:
        return 1 - n

    if n == 3:
        return 2

    if n % 2 == 0:
        return 1 + answer2(n / 2)

    return 1 + (answer2(n + 1) if count_to_1(n + 1) > count_to_1(n - 1) else answer2(n - 1))
