"""Sort ascending versions."""

l = ["1.11", "2.0.0", "1.2", "2", "0.1", "1.2.1", "1.1.1", "2.0"]
testL = ["0.1", "1.1.1", "1.2", "1.2.1", "1.11", "2", "2.0", "2.0.0"]


def answer(l):
    """Doc."""
    l.sort(key=lambda s: [int(u) for u in s.split('.')])
    if testL == l:
        print('lists match.')
    return l


answer(l)
