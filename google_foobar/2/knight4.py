"""Chessboard. Knight. Shortest number of moves, any square to any other."""
import math


grid = 64
gridXYlen = int(math.sqrt(grid))
valid_max = gridXYlen - 1
grid_max = grid - 1
m = [[-1, -2], [-2, -1], [-2,  1], [-1,  2], [1,  2], [2,  1], [2, -1], [1, -2]]


class XY:
    """Coordinate class."""

    def __init__(self, x_, y_):
        """Doc."""
        self.x = x_
        self.y = y_

    def __repr__(self):
        """Doc."""
        return str(self.x) + " " + str(self.y)


def n2xy(n):
    """Number to coordinate."""
    x = n % gridXYlen
    y = n / gridXYlen
    return XY(x, y)


def xy2n(xy):
    """Coordinate to number."""
    x = xy.x
    y = xy.y
    return x + (gridXYlen * y)


def answer(src, dest):
    """Answer."""
    global b
    b = {}
    # print('in answer: ', src, dest)
    if src < 0 or src > grid_max or dest < 0 or dest > grid_max:
        exit()
    s = n2xy(src)
    d = n2xy(dest)
    # print('s, d: ', s, d)
    r = srch(s, d)
    # print('solution: : ', r - 1)
    del globals()['b']
    return r - 1


def srch(start, end):
    """Search."""
    # print('in srch: ', start, end)
    queue = []
    queue.append(start)
    while queue:
        tmp_xy = queue.pop(0)
        # print(tmp_xy)
        if (tmp_xy.x == end.x and tmp_xy.y == end.y):
            return bt(tmp_xy, start)
        else:
            tmp_arr = next_m(tmp_xy, end)
        # comment to test against old method
        # if tmp_arr['stop'] == [True, xy2n(end)]:
        #     tmp_xy = n2xy(tmp_arr['stop'][1])
        #     return bt(tmp_xy, start)
        # else:
        #     queue.extend(tmp_arr['arr'])
            queue.extend(tmp_arr['arr'])


def next_m(xy, end):
    """Next Moves."""
    t = []
    arr = []
    for ia in xrange(len(m)):
        for ib in xrange(len(m[ia])):
            add_xy(xy, m[ia][0], m[ia][1], arr, end, t)

    tmp_arr = {'arr': arr, 'stop': t}
    return tmp_arr


def add_xy(xy, x_off, y_off, arr, end, t):
    """Add coordinates."""
    x = xy.x
    y = xy.y
    x_ = x + x_off
    y_ = y + y_off
    nw_xy = XY(x_, y_)
    if xy2n(nw_xy) == xy2n(end):
        if v(nw_xy):
            t.append(True)
            t.append(xy2n(nw_xy))
            arr.append(nw_xy)
            smr(xy, nw_xy)
    else:
        if v(nw_xy):
            arr.append(nw_xy)
            smr(xy, nw_xy)
    return arr


def v(xy):
    """Verify."""
    x = xy.x
    y = xy.y
    k = str(x) + str(y)
    if x < 0 or y < 0 or x > valid_max or y > valid_max:
        return False
    else:
        if k in b:
            # print('k in b:' + str(b[k]) + '!!')
            return False
        else:
            # print xy
            return True


def smr(o, nw):
    """Set Most Recent."""
    x = nw.x
    y = nw.y
    k = str(x) + str(y)
    b[k] = o


def bt(xy, start):
    """Backtrack."""
    arr = []
    arr.append(xy)
    while not(xy.x == start.x and xy.y == start.y):
        k = str(xy.x) + str(xy.y)
        x = b[k].x
        y = b[k].y
        arr.append(XY(x, y))
        xy = XY(x, y)
    # print(arr)
    for ia in xrange(len(arr)):
        arr[ia] = xy2n(arr[ia])
    return len(arr)

#
# def steps(o_rslt):
#     # nw_rslt = []
#     # print(o_rslt)
#     # for i in range(0, len(o_rslt)):
#     #     nw_rslt.append(xy2n(o_rslt[i]))
#     # nw_rslt.reverse()
#     # output = len(o_rslt)
#     return type(len(o_rslt) - 1)


def fuzz():
    """Doc."""
    src = range(0, 64)
    dest = range(0, 64)
    dest.reverse()
    fuzz_list = zip(src, dest)
    for i in xrange(len(fuzz_list)):
        s = int(fuzz_list[i][0])
        d = int(fuzz_list[i][1])
        print s, d
        print('solution: ' + str(answer(s, d)))


fuzz()

# src = 19
# dest = 36
# print(answer(src, dest))
# print(answer(src, dest))
