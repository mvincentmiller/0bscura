"""DFS rewrite for 4.1."""
import itertools
from collections import defaultdict

class Game:
    """Doc."""

    game = []


def s(x, y):
    """Step."""
    while(y):
        x, y = y, x % y
    return x

def e(x, y):
    """Evaluate."""
    if x is y:
        return 0

    l = s(x, y)

    if (x+y) % 2 == 1:
        return 1

    x, y = x/l, y/l
    x, y = max(x, y), min(x, y)
    return e(x-y, 2 * y)


def model(g):
    """Model the gambling guards."""
    if (g[0] + g[1]) % 2 == 1:
        return 0
    if (g[0] + g[1]) % 2 == 0:
        d = e(g[0], g[1])
        if d is 1:
            return 0
        if d is 0:
            return 1


def model2(node, opponents):
    """Doc."""
    for o in opponents:
        Game.game.append(model([node, o]))



def visit(node, graph, visited):
    """Visit node."""
    if node not in visited:
        opponents = [x for x in graph[node] if x not in visited]
        model2(node, opponents)
        visited.append(node)
        for c in graph[node]:
            visit(c, graph, visited)


def traverse(graph, visited, start):
    """Begin traversal."""
    # print graph.keys()
    # if graph.keys() is []:
    #     raw_input('Error! ...?')
    node = start
    try:
        visit(node, graph, visited)
    except IndexError:
        print 'Error!'
        print graph.keys()
        raw_input('?')

def nestTuples(t):
    """Format for adjacent egde list style."""
    return [nestTuples(i) for i in t] if isinstance(t, (list, tuple)) else t


def answer3(banana_list):
    """Doc."""
    Game.game = []

    adj = list(itertools.combinations(banana_list, 2))
    adj = nestTuples(adj)
    graph = defaultdict(list)

    visited = []
    if len(banana_list) is 1:
        return int(1)
    if len(set(banana_list)) <= 1:
        return len(banana_list)

    for edge in adj:
        graph[edge[0]].append(edge[1])
        graph[edge[1]].append(edge[0])
    try:
        chk = graph.keys()[0] + 1
    except IndexError:
        print banana_list
        print graph.keys()
        raw_input('Error! ...?')

    start = banana_list[0]

    traverse(graph, visited, start)

    if 0 in Game.game:
        return 0
    else:
        return len(banana_list)


print answer3([3,4,2,6,2]);
