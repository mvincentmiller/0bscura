"""Answers for 4.1."""
import itertools


def model(g):
    """Model the gambling guards."""
    i = 1
    h = []
    while i:
        # if g[0] >= 1024 or g[1] >= 1024:
        #     return 0
        if g[0] is g[1]:
            return 1
        else:
            # print 'guards distracted...'
            loser = max(g) - min(g)
            winner = min(g) + min(g)
            g = [winner, loser]
            # print h
            if g[0] in h or g[1] in h:
                return 0
                # return g
            else:
                h.append(g[0])
                h.append(g[1])
                # print g


def makePairs(l):
    """Find all combinations of two."""
    return list(itertools.combinations(l, 2))


def playRound(l):
    """Return modeled list, leaving off duplicates."""
    # return [model(z) for z in l if z[0] is not z[1]]
    return [model(z) for z in l]


# def flatten(l):
#     """Flatten nested arrays."""
#     print 'finding a new player...'
#     raw_input('OK?')
#     return list(itertools.chain.from_iterable(l))


def referee(banana_list):
    """Pair up and play a rounds."""
    a = makePairs(banana_list)
    print(a)
    b = playRound(a)
    if 0 in b:
        return 0
    if 1 in b:
        return len(banana_list)
    else:
        b = flatten(b)
        b = referee(b)
    return b


def answer(banana_list):
    """Compute answer."""
    # print banana_list
    if len(banana_list) is 1:
        # print 'end game: one player'
        return int(1)
    if len(set(banana_list)) <= 1:
        return len(banana_list)
    return referee(banana_list)
