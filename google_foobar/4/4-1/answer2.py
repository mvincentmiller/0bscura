"""DFS Answer for 4.1."""


class Graph:
    """DFS Graph Class."""

    def __init__(self, banana_list):
        """Init function."""
        # print 'init.'
        self.list_len = len(banana_list)
        self.graph = list([0] * self.list_len for i in xrange(self.list_len))
        # print self.graph
        for i in xrange(self.list_len):
            for j in xrange(self.list_len):
                if i < j:
                    # # print i, j
                    # print 'banana_list[i], banana_list[j]: ', [banana_list[i], banana_list[j]]
                    self.graph[i][j] = self.dead_lock(banana_list[i], banana_list[j])
                    # print ':  ', self.graph[i][j]
                    self.graph[j][i] = self.graph[i][j]


    def gcd(self, x, y):
        """Reduce."""
        while(y):
            x, y = y, x % y
            # # print x, y
        return x

    def dead_lock(self, x, y):
        """Dead lock."""

        if x is y:
            return 0

        l = self.gcd(x, y)

        if (x+y) % 2 == 1:
            return 1

        x, y = x/l, y/l
        x, y = max(x, y), min(x, y)
        # # print '-> ', [x, y]
        return self.dead_lock(x-y, 2 * y)


    def bpm(self, i, matchR, seen):
        """A DFS based recursive function that returns true if a matching for vertex u is possible."""
        # # print '....'
        for v in range(self.list_len):
            if self.graph[i][v] and seen[v] is False:
                seen[v] = True  # Mark v as visited
                if matchR[v] == -1 or self.bpm(matchR[v], matchR, seen):
                    matchR[v] = i
                    return True
        return False


    def maxGaurdPair(self):
        """Returns maximum number of matching."""
        # print "..."
        # print self.graph
        matchR = [-1] * self.list_len
        # print 'matchR ', matchR
        result = 0  # Count of graud match
        for i in range(self.list_len):
            seen = [False] * self.list_len
            if self.bpm(i, matchR, seen):
                result += 1
                # print 'result ', result
        return self.list_len - 2 * (result/2)


def answer2(l):
    """Return answer."""
    # # print l
    return Graph(l).maxGaurdPair()

# print answer2([1, 7, 3, 29, 13, 19])
# # print answer2([6, 1, 8])
# # print answer2([3,4,2,6,2])
