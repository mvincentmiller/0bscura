"""Answer 4 for 4.1."""
import itertools
from collections import defaultdict


class Game:
    """Doc."""
    game = []
    opponents = []
    r = []
    graph = {}


def removeFromGraph(node, o):
    """Doc."""
    print '...'
    # print Game.graph
    # print Game.r


def play(node):
    """Play opponents."""
    for o in Game.opponents:
        # print [node, o]
        if node is o:
            removeFromGraph(node, o)
            # print '2 are left'
            # Game.opponents.pop(o)
            # remove node and o from graph, leave in those 2 in r
            # Game.r.append(1)
            # Game.r.append(1)
        if node % o == 1:
            # remove looping node and o from graph, remove from r
            removeFromGraph(node, o)


def visit(node, graph, visited):
    """Visit node."""
    # print graph[node]
    if node not in visited:
        print node
        # Game.opponents = [x for x in graph[node] if x not in visited]
        # print node, Game.opponents
        # play(node)
        visited.append(node)
        for c in graph[node]:
            visit(c, graph, visited)


def traverse(graph, visited):
    """Begin graph traversal."""
    node = graph.keys()[0]
    print node
    try:
        visit(node, graph, visited)
    except IndexError:
        print 'Error!'


def nestTuples(t):
    """Format for adjacent egde list style."""
    return [nestTuples(i) for i in t] if isinstance(t, (list, tuple)) else t


def answer4(banana_list):
    """Doc."""
    Game.r = banana_list
    Game.game = []

    adj = list(itertools.combinations(banana_list, 2))
    adj = nestTuples(adj)
    Game.graph = defaultdict(enumerate(list))
    visited = []
    if len(banana_list) is 1:
        return int(1)
    if len(set(banana_list)) <= 1:
        return len(banana_list)
    print adj
    for edge in adj:
        Game.graph[edge[0]].append(edge[1])
        Game.graph[edge[1]].append(edge[0])
    print Game.graph

    traverse(Game.graph, visited)

    if 0 in Game.game:
        return 0
    else:
        return len(banana_list)


# banana_list2 = [1, 2, 3, 1, 2, 3]
banana_list2 = [6, 1, 8]
banana_list = [1, 7, 3, 21, 13, 19]

visited = []
d = dict(enumerate(banana_list2))
print d
traverse(d, visited)

#
# for b in banana_list:
#     print b

# print answer4(banana_list)
# print answer4(banana_list2)
