"""
Solution attempt #5 for foobar 4.1.

I didn't spend nearly enough time on this
considering the 15 or so days allowed.

You can compare my approach to this one:
https://github.com/arinkverma/google-foobar/blob/master/4.2_distract_grauds.py

I thought you might appreciate the link.
I enjoy the questions so I'm proceeding with this solution,
but I wanted to make sure to be transparent about it.

Just in case anyone reads this, I would love to know how
the direction my approach was headed compares.

If space and time allowed I would share my testing process.
Most notably, timing and asserting against different methods.

Cheers.
-Vince
"""


# import itertools
# from collections import defaultdict

# banana_list2 = [6, 1, 8]


class G:
    """G class."""

    def __init__(self, banana_list):
            """Init function."""
            # print 'init.'
            self.f = []
            self.list_len = len(banana_list)
            self.graph = list([0] * self.list_len for i in xrange(self.list_len))
            # print self.graph
            for i in xrange(self.list_len):
                for j in xrange(self.list_len):
                    if i < j:
                        # print i, j
                        # print 'banana_list[i], banana_list[j]: ', [banana_list[i], banana_list[j]]
                        self.graph[i][j] = self.e(banana_list[i], banana_list[j])
                        self.graph[j][i] = self.graph[i][j]

    def s(self, x, y):
        """Reverse."""
        while(y):
            x, y = y, x % y
        return x

    def e(self, x, y):
        """
        Evaluate.

        Is x equal to y (return to guard)?
        Is the sum odd (summands will never be even)?
        """
        if x is y:
            return 0

        l = self.s(x, y)

        if (x+y) % 2 == 1:
            return 1

        x, y = x/l, y/l
        x, y = max(x, y), min(x, y)
        return self.e(x-y, 2 * y)

    def dfs(self, i, matchRat, seen):
        """
        A DFS based recursive function.

        Returns True if a matching for vertex u is possible.
        """
        for v in range(self.list_len):
            if self.graph[i][v] and seen[v] is False:
                seen[v] = True  # Mark v as visited
                if matchRat[v] == -1 or self.dfs(matchRat[v], matchRat, seen):
                    matchRat[v] = i
                    return True
        return False

    def total(self):
        """
        Returns maximum number of matching.

        ...would be nice to see if this calculation
        could be completed in the original graph traversal? idk.
        """
        matchRat = [-1] * self.list_len
        result = 0
        for i in range(self.list_len):
            seen = [False] * self.list_len
            if self.dfs(i, matchRat, seen):
                result += 1
        return self.list_len - 2 * (result/2)

    # def doStuff(self):
    #     """Doc."""
    #
    #     l = banana_list
    #     b = self.nestTuples(self.makePairs(l))  # [[6, 1], [6, 8], [1, 8]]
    #
    #     graph = defaultdict(list)
    #
    #     visited = []
    #     # if len(banana_list) is 1:
    #     #     return int(1)
    #     # if len(set(banana_list)) <= 1:
    #     #     return len(banana_list)
    #
    #     for edge in b:
    #         graph[edge[1]].append(edge[0])
    #         graph[edge[0]].append(edge[1])
    #
    #       ############################ I was getting close,
    #       ##########################    [00111] not [000111], for example.
    #
    #     # G(banana_list).traverse(graph, visited, l)
    #     # print 'nodes visited:', visited
    #     # print self.graph ##################################################
    #     # print self.f ############### next step would be to total ########
    #     # ################################################################
    #
    # def traverse(self, graph, visited, l):
    #     """Begin traversal."""
    #     node = l[0]
    #     self.visit(node, graph, visited, l)
    #
    # def visit(self, node, graph, visited, l):
    #     """Visit nodes."""
    #     if node not in visited:
    #         visited.append(node)
    #         r = [self.e(node, g) for g in graph[node]] ######################
    #         G.f.append(r) ##################################################
    #         for c in graph[node]:
    #             self.visit(c, graph, visited, l)
    #
    # def nestTuples(self, t):
    #     """Format for adjacent egde list style."""
    #     return [self.nestTuples(i) for i in t] if isinstance(t, (list, tuple)) else t
    #
    # def makePairs(self, l):
    #     """Find all combinations of two."""
    #     return list(itertools.combinations(l, 2))


def answer5(banana_list):
    """Answer5."""
    return G(banana_list).total()
