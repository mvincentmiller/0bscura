"""Tests for 4.1."""
import random
import timeit
# from answer import answer
from answer2 import answer2
from answer3 import answer3
from answer5 import answer5
maxPlayers = 101
maxBananas = 1073741823


# banana_list = random.sample(xrange(1, maxBananas), random.randint(1, maxPlayers))
banana_list1 = [1, 1]
banana_list2 = [1, 7, 3, 21, 13, 19]
banana_list3 = [1, 1, 7]
banana_list4 = random.sample(xrange(1, maxBananas), 100)
banana_list5 = [1, 1, 1, 7]



def test(a, n):
    """Doc."""
    if n == banana_list1:
        start = timeit.default_timer()
        r = a(n)
        stop = timeit.default_timer()
        time = stop - start
        # print r
        if r == 2:
            return [r, True, time]
        else:
            return [False, r]


def test2(a, n):
    """Doc."""
    if n == banana_list2:
        start = timeit.default_timer()
        r = a(n)
        stop = timeit.default_timer()
        time = stop - start
        # print r
        if r == 0:
            return [r, True, time]
        else:
            return [False, r]


def test3(a, n):
    """Doc."""
    if n == banana_list3:
        start = timeit.default_timer()
        r = a(n)
        stop = timeit.default_timer()
        time = stop - start
        # print r
        if r == 3:
            return [r, True, time]
        else:
            return [False, r]


def test4(a, n):
    """Doc."""
    if n == banana_list4:
        start = timeit.default_timer()
        r = a(n)
        stop = timeit.default_timer()
        time = stop - start
        return [r, 'Unknown', time]


def test5(a, n):
    """Doc."""
    if n == banana_list5:
        start = timeit.default_timer()
        r = a(n)
        stop = timeit.default_timer()
        time = stop - start
        # print r
        if r == 4:
            return [r, True, time]
        else:
            return [False, r]


def test6(a, n):
    """Doc."""
    if n == banana_list6:
        start = timeit.default_timer()
        r = a(n)
        stop = timeit.default_timer()
        time = stop - start
        return [r, 'Unknown', time]


def test7(a, b, n):
    """Compare."""
    # if n == banana_list6:
    r = a(n)
    r2 = b(n)
    return [r, r2]


def run_test3():
    global x
    print x
    print ['.'] * x
    # i = random.randint(1, 100)
    # j = random.randint(1, maxBananas)
    # l = [j] * i
    # l = [1, j] * i
    l = random.sample(xrange(1, 100), 50)

    # print l
    a2 = answer2(l)
    a3 = answer5(l)
    while a2 is a3:
        print '3 ok.'
        try:
            while x < testMax:
                x = x + 1
                run_test3()
            else:
                x = 0
                run_test()
        except RuntimeError:
            print '.......RUNTIME EXCEPTION.......'
            raw_input('?')
    else:
        print a2, a3
        raw_input('?')




def run_test2():
    global x
    print x
    print ['.'] * x
    i = random.randint(1, 10)
    j = random.randint(1, 10)
    k = random.randint(1, 10)
    l = [i, j, k] * i
    a2 = answer2(l)
    a3 = answer5(l)
    if a2 == a3:
        print '2 ok.'
        try:
            while x < testMax:
                x = x + 1
                run_test2()
            else:
                x = 0
                run_test3()
        except RuntimeError:
            raw_input('?')
    else:
        print len(l), l, len(l) / a2
        print a2 == a3
        print [a2 is a3, a2, a3, 'TEST 2 FAILED.']
        raw_input('?')


global x
x = 0

def run_test():
    """Test runner."""
    global x
    print ['.'] * x
    try:
        banana_list6 = random.sample(xrange(1, random.randint(12, maxBananas)), 100)
    except ValueError:
        banana_list6 = random.sample(xrange(1, 10), 6)
    # print banana_list6


    # print 'Answer 2: \n'
    # print test3(answer2, banana_list3)
    # print test4(answer2, banana_list4)
    # print test5(answer2, banana_list5)
    # print test(answer2, banana_list1)
    # print test2(answer2, banana_list2)
    # print '\n'
    # # print 'Answer 5: \n'
    # print test3(answer3, banana_list3)
    # print test4(answer3, banana_list4)
    # print test5(answer3, banana_list5)
    # print test(answer3, banana_list1)
    # print test2(answer3, banana_list2)

    a2 = answer2(banana_list6)
    a3 = answer5(banana_list6)
    while a2 is a3:
        print '1 ok!'
        try:
            x = x + 1
            print x
            if x < testMax:
                run_test()
            else:
                x = 0
                run_test2()
        except RuntimeError:
            raw_input('?')
    else:
        print a2, a3
        raw_input('?')

testMax = 3
run_test()
