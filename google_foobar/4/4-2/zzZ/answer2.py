#
# Ford and Fulkerson Algorithm implemented in Python.
#

class Edge(object):
    def __init__(self, source, sink, capacity):
        self.source = source
        self. sink = sink
        self.capacity = capacity

    def __repr__(self):
        return "%s -> %s : %d" % (self.source, self.sink, self.capacity)

class Flow(object):

    def __init__(self):
        self.edges = {}
        self.adjacents = {}

    # def add_edges(self, source, sink, capacity):
    def add_edges(self, m):
        source = str(m[0])
        sink = str(m[1])
        capacity = m[2]
        if source == sink:
            print source, sink
            raise ValueError("Source can not be the Sink.")
        edge = Edge(source, sink, capacity)
        redge = Edge(source, sink, capacity)
        self.edges[edge] = 0
        self.edges[redge] = 0

        if source not in self.adjacents:
            self.adjacents[source] = []
        if sink not in self.adjacents:
            self.adjacents[sink] = []

        self.adjacents[source].append(edge)
        self.adjacents[sink].append(redge)

    def valid_path(self, source, sink, path):
        """ Returns the list of the edges from source to sink """

        if source == sink:
            return path
        for edge in self.adjacents[source]:
            if edge not in path:
                if edge.capacity - self.edges[edge] > 0:
                    return self.valid_path(edge.sink, sink, path + [edge])

        # In case there is no more possible path:
        return None

    def max_flow(self, source, sink):
        """" Update the flow for edges and returns the max_flow """

        path = self.valid_path(source, sink, [])

        while (path):
            # get the maximum possible flow that can be taken from this path:
            max_flow = min([edge.capacity for edge in path])
            for edge in path:
                self.edges[edge] += max_flow
            path = self.valid_path(source, sink, [])

        # Compute all the flows from the neighbors of source:
        return  sum([self.edges[edge] for edge in self.adjacents[source]])


t = Flow()

# t.add_edges('A', 'C', 4)
# t.add_edges('A', 'D', 6)
# t.add_edges('B', 'C', 5)
# t.add_edges('B', 'D', 2)
# t.add_edges('C', 'E', 4)
# t.add_edges('C', 'F', 4)
# t.add_edges('D', 'E', 6)
# t.add_edges('D', 'F', 6)
#
# print t.max_flow('A', 'E') + t.max_flow('B', 'F')


# t.add_edges('A', 'B', 7)
# t.add_edges('A', 'C', 0)
# t.add_edges('B', 'D', 6)
# t.add_edges('C', 'D', 8)
#
# print t.max_flow('A', 'D')

# t.add_edges(0, 1, 7)
# t.add_edges(0, 2, 0)
# t.add_edges(1, 3, 6)
# t.add_edges(2, 3, 8)
#
# print t.max_flow(0, 3)




e = [0]
x = [3]
p = [
[0, 7, 0, 0], # 0 sends 6/7 to 2
[0, 0, 0, 6], # 1 sends 6/6 to 3
[0, 0, 0, 8],
[9, 0, 0, 0]
]

e2 = [0, 1]
x2 = [4, 5]
p2 = [
[0,0,4,6,0,0], # 0 sends 4/4 bunnies to 2 and 6/6 bunnies to 3
[0,0,5,2,0,0], # 1 sends 4/5 bunnies to 2 and 2/2 bunnies to 3
[0,0,0,0,4,4], # 2 sends 4/4 bunnies to 4 and 4/4 bunnies to 5
[0,0,0,0,6,6], # 3 sends 4/6 bunnies to 4 and 4/6 bunnies to 5
[0,0,0,0,0,0],
[0,0,0,0,0,0]
]


def removeZeros(p):
    """Doc."""
    for q in p:
        p[p.index(q)] = [i for i in p[p.index(q)] if i != 0]
    return p


def rooms(nodes, e, x):
    """Doc."""
    r = set(nodes) - set(e) - set(x)
    return list(r)


def build_graph(e, x, r, p, gr):
    """Doc."""


    m = []

    # entrance -> rooms
    for i in e:
        for s in r:
            m.append([i, s, gr[i]])

    # rooms -> exits
    for s in r:
        for z in x:
            m.append([s, z, gr[s]])

    for n in m:
        # print n
        if n[0] is 0:
            print n
        t.add_edges(n)

    #
    # t.add_edges('0', '1', 7)
    # t.add_edges('0', '2', 0)
    # t.add_edges('1', '3', 6)
    # t.add_edges('2', '3', 8)
    #
    # print t.max_flow('0', '3')








def answer(e, x, p):
    """Doc."""
    nodes = list(xrange(len(p)))
    # nodes = [str(n) for n in nodes]
    print nodes

    # print 'nodes: ', nodes
    r = rooms(nodes, e, x)
#
    removeZeros(p)
    # print 'entrance, rooms, exits: ', e, r, x


    gr = dict(zip(nodes, p))
    print 'gr', gr
    # for g in gr:
    #     print gr[g]
    build_graph(e, x, r, p, gr)
    # for i in e:
    #     for z in x:
    #         print t.max_flow(i, z)


# print answer(e, x, p)
# print answer(e2, x2, p2)

graph = {'0': set(['2', '3']),
         '1': set(['2', '3']),
         '2': set(['4', '5']),
         '3': set(['4', '5']),
         '4': set([]),
         '5': set([])}

def bfs(graph, start):
    visited, queue = set(), [start]
    while queue:
        vertex = queue.pop(0)
        if vertex not in visited:
            print visited
            visited.add(vertex)
            queue.extend(graph[vertex] - visited)
    return visited

print bfs(graph, '0') 
print bfs(graph, '1')
