"""Answer for 4.2."""

# Escape Pods
# ===========

# You've blown up the LAMBCHOP doomsday device and broken the bunnies out of Lambda's prison - and now you need to escape from the space station as quickly and as orderly as possible! The bunnies have all gathered in various locations throughout the station, and need to make their way towards the seemingly endless amount of escape pods positioned in other parts of the station. You need to get the numerous bunnies through the various rooms to the escape pods. Unfortunately, the corridors between the rooms can only fit so many bunnies at a time. What's more, many of the corridors were resized to accommodate the LAMBCHOP, so they vary in how many bunnies can move through them at a time.

# Given the starting room numbers of the groups of bunnies, the room numbers of the escape pods, and how many bunnies can fit through at a time in each direction of every corridor in between, figure out how many bunnies can safely make it to the escape pods at a time at peak.
#
# Write a function answer(entrances, exits, path) that takes an array of integers denoting where the groups of gathered bunnies are, an array of integers denoting where the escape pods are located, and an array of an array of integers of the corridors, returning the total number of bunnies that can get through at each time step as an int. The entrances and exits are disjoint and thus will never overlap. The path element path[A][B] = C describes that the corridor going from A to B can fit C bunnies at each time step.  There are at most 50 rooms connected by the corridors and at most 2000000 bunnies that will fit at a time.
#
# For example, if you have:
# entrances = [0, 1]
# exits = [4, 5]
# path = [
#   [0, 0, 4, 6, 0, 0],  # Room 0: Bunnies
#   [0, 0, 5, 2, 0, 0],  # Room 1: Bunnies
#   [0, 0, 0, 0, 4, 4],  # Room 2: Intermediate room
#   [0, 0, 0, 0, 6, 6],  # Room 3: Intermediate room
#   [0, 0, 0, 0, 0, 0],  # Room 4: Escape pods
#   [0, 0, 0, 0, 0, 0],  # Room 5: Escape pods
# ]
#
# Then in each time step, the following might happen:
# 0 sends 4/4 bunnies to 2 and 6/6 bunnies to 3
# 1 sends 4/5 bunnies to 2 and 2/2 bunnies to 3
# 2 sends 4/4 bunnies to 4 and 4/4 bunnies to 5
# 3 sends 4/6 bunnies to 4 and 4/6 bunnies to 5
#
# So, in total, 16 bunnies could make it to the escape pods at 4 and 5 at each time step.  (Note that in this example, room 3 could have sent any variation of 8 bunnies to 4 and 5, such as 2/6 and 6/6, but the final answer remains the same.)
#
# Languages
# =========
#
# To provide a Python solution, edit solution.py
# To provide a Java solution, edit solution.java
#
# Test cases
# ==========
#
# Inputs:
#     (int list) entrances = [0]
#     (int list) exits = [3]
#     (int) path = [[0, 7, 0, 0], [0, 0, 6, 0], [0, 0, 0, 8], [9, 0, 0, 0]]
# Output:
#     (int) 6
#
# Inputs:
#     (int list) entrances = [0, 1]
#     (int list) exits = [4, 5]
#     (int) path = [[0, 0, 4, 6, 0, 0], [0, 0, 5, 2, 0, 0], [0, 0, 0, 0, 4, 4], [0, 0, 0, 0, 6, 6], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
# Output:
#     (int) 16


entrances = [0]
exits = [3]
path = [
[0, 7, 0, 0], # 0 sends 6/7 to 2
[0, 0, 0, 6], # 1 sends 6/6 to 3
[0, 0, 0, 8], # empty cooridor
[9, 0, 0, 0]
]

helpString = """
path = [
[0, 7, 0, 0], # 0 sends 6/7 to 2
[0, 0, 6, 0], # 1 sends 6/6 to 3
[0, 0, 0, 8], # empty cooridor
[9, 0, 0, 0]
]
"""

# output 6


entrances2 = [0, 1]
exits2 = [4, 5]

path2 = [
[0,0,4,6,0,0], # 0 sends 4/4 bunnies to 2 and 6/6 bunnies to 3
[0,0,5,2,0,0], # 1 sends 4/5 bunnies to 2 and 2/2 bunnies to 3
[0,0,0,0,4,4], # 2 sends 4/4 bunnies to 4 and 4/4 bunnies to 5
[0,0,0,0,6,6], # 3 sends 4/6 bunnies to 4 and 4/6 bunnies to 5
[0,0,0,0,0,0],
[0,0,0,0,0,0]
]

# output 16


def findBunnies(e, p):
    """Doc."""
    esc = []
    for i in e:
        p[i] = [i for i in p[i] if i != 0]
        esc.append(p[i])
    if len(esc) is 1:
        esc = esc[0]
    return esc


def findCooridors(e, x, p):
    """Doc."""
    return [p[max(e) + 1], p[min(x) - 1]]


def removeZeros(c):
    """Doc."""
    for d in c:
        c[c.index(d)] = [i for i in c[c.index(d)] if i != 0]
    return c


def operator(b, c):
    """Doc."""
    d = sum(c, [])
    if len(b) is 1:
        if b[0] <= d[0]:
            d[0] = b[0]
        else:
            return [d[0]]
    for x in b:
        for i in x:
            # print str(x.index(i)) + ': ', i,  c[x.index(i)][x.index(i)]
            if i <= c[x.index(i)][x.index(i)]:
                c[x.index(i)][x.index(i)] = i
    return c


def answer(entrances, exits, path):
    """Doc."""
    e = entrances
    x = exits
    p = path
    for q in p:
        if p.index(q) in e:
            print p.index(q), q, 'entrance', 'bunnies: ', [i for i in q if i != 0]
        elif p.index(q) in x:
            print p.index(q), q, 'exit'
        else:
            print p.index(q), q, 'cooridor'


    # print '\n'
    b = findBunnies(e, p)
    # print 'findBunnies: ', b
    c = findCooridors(e, x, p)
    c = removeZeros(c)
    # print 'findCooridors: ', c
    t = operator(b, c)
    # print 'operator: ', t
    if len(t) is 1:
        return t[0]
    f = [i for s in t for i in s]
    # print '\n Output: \n'
    return sum(f)

print answer(entrances, exits, path)
print answer(entrances2, exits2, path2)
