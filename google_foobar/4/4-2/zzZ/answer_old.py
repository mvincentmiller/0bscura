"""Answer for 4.2."""

# Escape Pods
# ===========

# You've blown up the LAMBCHOP doomsday device and broken the bunnies out of Lambda's prison - and now you need to escape from the space station as quickly and as orderly as possible! The bunnies have all gathered in various locations throughout the station, and need to make their way towards the seemingly endless amount of escape pods positioned in other parts of the station. You need to get the numerous bunnies through the various rooms to the escape pods. Unfortunately, the corridors between the rooms can only fit so many bunnies at a time. What's more, many of the corridors were resized to accommodate the LAMBCHOP, so they vary in how many bunnies can move through them at a time.

# Given the starting room numbers of the groups of bunnies, the room numbers of the escape pods, and how many bunnies can fit through at a time in each direction of every corridor in between, figure out how many bunnies can safely make it to the escape pods at a time at peak.
#
# Write a function answer(entrances, exits, path) that takes an array of integers denoting where the groups of gathered bunnies are, an array of integers denoting where the escape pods are located, and an array of an array of integers of the corridors, returning the total number of bunnies that can get through at each time step as an int. The entrances and exits are disjoint and thus will never overlap. The path element path[A][B] = C describes that the corridor going from A to B can fit C bunnies at each time step.  There are at most 50 rooms connected by the corridors and at most 2000000 bunnies that will fit at a time.
#
# For example, if you have:
# entrances = [0, 1]
# exits = [4, 5]
# path = [
#   [0, 0, 4, 6, 0, 0],  # Room 0: Bunnies
#   [0, 0, 5, 2, 0, 0],  # Room 1: Bunnies
#   [0, 0, 0, 0, 4, 4],  # Room 2: Intermediate room
#   [0, 0, 0, 0, 6, 6],  # Room 3: Intermediate room
#   [0, 0, 0, 0, 0, 0],  # Room 4: Escape pods
#   [0, 0, 0, 0, 0, 0],  # Room 5: Escape pods
# ]
#
# Then in each time step, the following might happen:
# 0 sends 4/4 bunnies to 2 and 6/6 bunnies to 3
# 1 sends 4/5 bunnies to 2 and 2/2 bunnies to 3
# 2 sends 4/4 bunnies to 4 and 4/4 bunnies to 5
# 3 sends 4/6 bunnies to 4 and 4/6 bunnies to 5
#
# So, in total, 16 bunnies could make it to the escape pods at 4 and 5 at each time step.  (Note that in this example, room 3 could have sent any variation of 8 bunnies to 4 and 5, such as 2/6 and 6/6, but the final answer remains the same.)
#
# Languages
# =========
#
# To provide a Python solution, edit solution.py
# To provide a Java solution, edit solution.java
#
# Test cases
# ==========
#
# Inputs:
#     (int list) entrances = [0]
#     (int list) exits = [3]
#     (int) path = [[0, 7, 0, 0], [0, 0, 6, 0], [0, 0, 0, 8], [9, 0, 0, 0]]
# Output:
#     (int) 6
#
# Inputs:
#     (int list) entrances = [0, 1]
#     (int list) exits = [4, 5]
#     (int) path = [[0, 0, 4, 6, 0, 0], [0, 0, 5, 2, 0, 0], [0, 0, 0, 0, 4, 4], [0, 0, 0, 0, 6, 6], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
# Output:
#     (int) 16


entrances = [0]
exits = [3]
path = [
[0, 7, 0, 0], # 0 sends 6/7 to 2
[0, 0, 6, 0], # 1 sends 6/6 to 3
[0, 0, 0, 8], # empty cooridor
[9, 0, 0, 0]
]

# output 6


def answer(entrances, exits, paths):
    """Doc."""
    print "\n Test Case 1: \n"
    e = entrances
    x = exits
    p = path
    for q in p:
        print q
    # bunnies are in path[entrance], path[entrance]...
    esc = []
    for i in e:
        for b in p[i]:
            if b > 0:
                esc.append(b)
    # print esc
    total = sum(esc)
    # bunnies step through the corridors between path[entrances] & path[exits]
    # range ...
    c = [p[max(e) + 1], p[min(x) - 1]]
    c[0] = [i for i in c[0] if i != 0]
    c[1] = [i for i in c[1] if i != 0]
    for b in esc:
        if esc.index(b) is 0:
            if not b <= c[0][0]:
                print 'lost a bunny'
                total = total - 1
            else:
                c[0][0] = b
        elif esc.index(b) % 2 is 0:
            print b, c[0]
        if esc.index(b) % 2 is not 0:
            print b, c[0]
    return total
# print answer(entrances, exits, path)


entrances2 = [0, 1]
exits2 = [4, 5]

path2 = [
[0,0,4,6,0,0], # 0 sends 4/4 bunnies to 2 and 6/6 bunnies to 3
[0,0,5,2,0,0], # 1 sends 4/5 bunnies to 2 and 2/2 bunnies to 3
[0,0,0,0,4,4], # 2 sends 4/4 bunnies to 4 and 4/4 bunnies to 5
[0,0,0,0,6,6], # 3 sends 4/6 bunnies to 4 and 4/6 bunnies to 5
[0,0,0,0,0,0],
[0,0,0,0,0,0]
]

# output 16


def answer2(entrances, exits, path):
    """Doc."""
    print "\n Test Case 2: \n"
    e = entrances
    x = exits
    p = path
    for q in p:
        print q
    # bunnies are in path[entrance], path[entrance]...
    esc = []
    for i in e:
        for b in p[i]:
            if b > 0:
                esc.append(b)
    # print esc
    total = sum(esc)
    # bunnies step through the corridors between path[entrances] & path[exits]
    # range ..
    #find cooridors from paths.
    c = [p[max(e) + 1], p[min(x) - 1]]
    print c
    c[0] = [i for i in c[0] if i != 0]
    c[1] = [i for i in c[1] if i != 0]
    for b in esc:
        if esc.index(b) is 0:
            # print b, c[0][0]
            if not b <= c[0][0]:
                # print 'lost a bunny'
                total = total - 1
            else:
                c[0][0] = b
        elif esc.index(b) % 2 is 0:
            # print b, c[0][1]
            if not b <= c[0][1]:
                # print 'lost a bunny'
                total = total - 1
            else:
                c[0][1] = b
        if esc.index(b) % 2 is not 0:
            # print b, c[1][0]
            if not b <= c[1][0]:
                # print 'lost a bunny'
                # TODO: difference of c[i] and b
                # not total here
                total = total - 1
            else:
                c[1][0] = b
        # print c
    return total





def findBunnies(e, p):
    """Doc."""
    esc = []
    for i in e:
        p[i] = [i for i in p[i] if i != 0]
        esc.append(p[i])
    return esc


def findCooridors(e, x, p):
    """Doc."""
    return [p[max(e) + 1], p[min(x) - 1]]


def removeZeros(c):
    """Doc."""
    for d in c:
        c[c.index(d)] = [i for i in c[c.index(d)] if i != 0]
    return c


def operator(b, c):
    """Doc."""
    for x in b:
        for i in x:
            # print str(x.index(i)) + ': ', i,  c[x.index(i)][x.index(i)]
            if i <= c[x.index(i)][x.index(i)]:
                c[x.index(i)][x.index(i)] = i
    return c

b = [[4,6],[5,2]]
c = [[4,4],[6,6]]
# print operator(b, c)

def answer3(entrances, exits, path):
    """Doc."""
    print '\n Input: \n '
    e = entrances
    x = exits
    p = path
    print 'entrances: ', e, '\n'
    print 'exits: ', x, '\n'
    print 'paths: '
    for q in p:
        print q
    print '\n'
    b = findBunnies(e, p)
    c = findCooridors(e, x, p)
    c = removeZeros(c)
    t = operator(b, c)
    f = [i for s in t for i in s]
    print 'output: \n'
    return sum(f)

print answer3(entrances2, exits2, path2)
