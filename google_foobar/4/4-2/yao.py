'''
RECRUITING NOTE: I WAS UNABLE TO COMPLETE 4-2, THIS IS THE WORK OF DOUG YAO!!!
IT WAS SUBMITTED AS SUCH AND ONLY TO CONTINUE THE LEARNING TRACK!
Douglas Yao https://github.com/douglasyao/Google-foobar/blob/master/level_4_problem_2.py
'''


import decimal
from collections import defaultdict


def EdmondsKarp(capacity, neighbors, start, end):
    flow = 0
    length = len(capacity)
    flows = [[0 for i in range(length)] for j in range(length)]
    while True:
        max, parent = BreadthFirstSearch(capacity, neighbors, flows, start, end)
        if max == 0:
            break
        flow = flow + max
        v = end
        while v != start:
            u = parent[v]
            flows[u][v] = flows[u][v] + max
            flows[v][u] = flows[v][u] - max
            v = u
    return flow


def BreadthFirstSearch(capacity, neighbors, flows, start, end):
    length = len(capacity)
    parents = [-1 for i in xrange(length)]
    parents[start] = -2
    M = [0 for i in xrange(length)]
    M[start] = decimal.Decimal('Infinity')

    queue = []
    queue.append(start)
    while queue:
        u = queue.pop(0)
        for v in neighbors[u]:
            if capacity[u][v] - flows[u][v] > 0 and parents[v] == -1:
                parents[v] = u
                M[v] = min(M[u], capacity[u][v] - flows[u][v])
                if v != end:
                    queue.append(v)
                else:
                    return M[end], parents
    return 0, parents


def collapse(entrances, exits, path):

    collapsed_entrances = [0 for x in range(len(path))]
    collapsed_exits = [0 for x in range(len(path))]
    direct_flow = 0

    for i in range(len(path)):
        for j in range(len(path[i])):
            if i in entrances and path[i][j] > 0:
                if j in exits:
                    direct_flow += path[i][j]
                    path[i][j] = 0
                else:
                    collapsed_entrances[j] += path[i][j]
            if j in exits and path[i][j] > 0:
                collapsed_exits[i] += path[i][j]

    if len(entrances) > 1:
        path[entrances[0]] = collapsed_entrances
        entrances.pop(0)
        for i in entrances:
            path[i] = [0 for x in range(len(path))]

    if len(exits) > 1:
        path = zip(*path)
        path[exits[0]] = collapsed_exits
        exits.pop(0)
        if exits:
            for i in exits:
                path[i] = [0 for x in range(len(path))]
        path = zip(*path)
    return path, direct_flow


def get_neighbors(path):
    neighbors = defaultdict(list)
    for vertex, flows in enumerate(path):
        for neighbor, flow in enumerate(flows):
            if flow > 0:
                neighbors[vertex].append(neighbor)
                neighbors[neighbor].append(vertex)
    return neighbors


def answer(entrances, exits, path):
    start = entrances[0]
    end = exits[0]
    capacity, direct_flow = collapse(entrances, exits, path)
    neighbors = get_neighbors(capacity)
    flow = EdmondsKarp(capacity, neighbors, start, end)
    return flow + direct_flow


e = [0]
x = [3]
p = [
[0, 7, 0, 0], # 0 sends 6/7 to 2
[0, 0, 0, 6], # 1 sends 6/6 to 3
[0, 0, 0, 8],
[9, 0, 0, 0]
]

e2 = [0, 1]
x2 = [4, 5]
p2 = [
[0,0,4,6,0,0], # 0 sends 4/4 bunnies to 2 and 6/6 bunnies to 3
[0,0,5,2,0,0], # 1 sends 4/5 bunnies to 2 and 2/2 bunnies to 3
[0,0,0,0,4,4], # 2 sends 4/4 bunnies to 4 and 4/4 bunnies to 5
[0,0,0,0,6,6], # 3 sends 4/6 bunnies to 4 and 4/6 bunnies to 5
[0,0,0,0,0,0],
[0,0,0,0,0,0]
]

print answer(e,x,p)
print answer(e2,x2,p2)
