#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64

MESSAGE = '''
FgkFHA0AAB0HHxNXDlEOHAYEGlMUE0pNGQUCBgQJAV0UTRRWTgsQEQsRVVYJCVpJSQYDCBtKRx4J VlNORAwAF0pWCUcUBQtESU5TWVAFRxMfCw4AAAAfE1cOURwADwoNH11XSgJWThwCBwwdTEBKDkxJ SRAECBEfH00JEAYBREVUVB9EBEBXThM=
'''

KEY = 'm.vincent83'

result = []
for i, c in enumerate(base64.b64decode(MESSAGE)):
    result.append(chr(ord(c) ^ ord(KEY[i % len(KEY)])))

print ''.join(result)
