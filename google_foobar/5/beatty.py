import math
import time

#
# Helpers for numbers in the form a + b*sqrt(2), encoded as a tuple (a,b)
#

# compare 2 numbers
def root2_cmp(x,y):
    x_a,x_b = x
    y_a,y_b = y

    a = x_a - y_a
    b = x_b - y_b

    a_sign = cmp(a,0)
    b_sign = cmp(b,0)

    if a_sign == b_sign: return a_sign

    if a_sign == 0: return b_sign
    if b_sign == 0: return a_sign

    # one of a_sign and b_sign is 1 and the other is -1
    # compare a^2 and (b*sqrt(2))^2 = 2*b^2

    aa = a*a
    bb2 = b*b*2

    # if a^2 > 2b^2, return a_sign otherwise the opposite sign
    return cmp(aa,bb2)*a_sign

def root2_max(x,y):
    return x if root2_cmp(x,y) > 0 else y

def root2_min(x,y):
    return x if root2_cmp(x,y) < 0 else y

def root2_add(x,y):
    return tuple(x_i+y_i for x_i,y_i in zip(x,y))

def root2_sub(x,y):
    return tuple(x_i-y_i for x_i,y_i in zip(x,y))

# Takes 2 sequence objects in the form (lower_bound, upper_bound, offset, sequence_data)
# Returns the same data for the concatenation of the sequences.

def seq_concat(a,b):
    a_lower, a_upper, a_offset, a_seq_data = a
    b_lower, b_upper, b_offset, b_seq_data = b

    c_lower = root2_max(a_lower, root2_sub(b_lower, a_offset))
    c_upper = root2_min(a_upper, root2_sub(b_upper, a_offset))

    # sanity check - should never be concatenating sequences that can't go together
    assert root2_cmp(c_upper, c_lower) > 0

    c_offset = root2_add(a_offset, b_offset)

    c_sequence = seq_data_concat(a_seq_data, b_seq_data)

    return c_lower, c_upper, c_offset, c_sequence

# Takes 2 sequence data objects in the form (length, slope, y-intercept)
# Returns the same data for the concatenation of the sequences.
def seq_data_concat(a,b):
    a_len, a_slope, a_intercept = a
    b_len, b_slope, b_intercept = b

    c_len = a_len + b_len
    c_slope = a_slope + b_slope
    c_intercept = a_slope*b_len + a_intercept + b_intercept

    return (c_len, c_slope, c_intercept)

# Compute the sum for i from 1 to n of floor(i*sqrt(2))
def root2_floor_sum(n):
    seqs = [
        #  lower  upper  offset  seq_data (length, slope, y-intercept)
        ( (0,0), (2,-1), (-1,1), (1,1,1) ),
        ( (2,-1), (1,0), (-2,1), (1,2,2) )
    ]

    prev_seq = None
    cur_seq_data = (0,0,0)
    cur_offset = (0,0)

    # while current sequence length is below n
    while cur_seq_data[0] < n:
        # remove too-big sequences
        max_len = n - cur_seq_data[0]
        seqs = filter(lambda (lb,ub,off,seq_data): seq_data[0] <= max_len, seqs)

        matching_seqs = filter(lambda (lb,ub,off,seq_data): root2_cmp(cur_offset, lb) >= 0 and root2_cmp(cur_offset, ub) < 0, seqs)
        next_seq = max(matching_seqs, key=lambda (lb,ub,off,seq_data): seq_data[0])

        if prev_seq != None:
            seq_to_add = seq_concat(prev_seq, next_seq)
            seqs.append(seq_to_add)

        next_lb, next_ub, next_off, next_seq_data = next_seq

        cur_seq_data = seq_data_concat(cur_seq_data, next_seq_data)
        cur_offset = root2_add(cur_offset, next_off)

        prev_seq = next_seq
    return cur_seq_data[2]

def verify_up_to(n):
    # verify the algorithm works for small n
    expected = 0
    root2 = math.sqrt(2)
    for i in range(1,n+1):
        expected += int(math.floor(root2*i))
        assert root2_floor_sum(i) == expected







def answer(n):
    n = int(n)
    result = root2_floor_sum(n)
    return result

# print result
# print 'computed in',int((end_time - start_time)*1000),'ms'
