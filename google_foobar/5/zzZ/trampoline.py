from math import floor, sqrt, ceil
# TailCall
#
# Takes a closure over function and creates a
# callable object. The closure object is not executed on its own,
# rather it is used as the the return value of a TailCallable object
# class TailCall:
#     def __init__(self, fn):
#         self.fn = fn
#     def __call__(self, *args):
#         return self.fn(*args)
#
# # TailCallable
# #
# # Takes a function which returns a TailCall, and
# # creates a callable object which mimics
# # a tail call optimized function.
#
# class TailCallable:
#     def __init__(self, fn):
#         self.fn = fn
#     def __call__(self, *args):
#         acc = self.fn(*args)
#         while (isinstance(acc, TailCall)):
#             acc = acc()
#         return acc
#
# def recursive_print(str):
#     # Create a temporary function
#     # which returns a recursive TailCall
#     # closure object
#     def rp(str):
#         if len(str) == 0:
#            return
#         print(str)
#         return TailCall(lambda: rp(str[1:]))
#     # Create and call a tail call optimized version
#     # of the temporary function
#     TailCallable(rp)(str)
#
# # recursive_print("hello")
#
#
# def recursive_beatty(n):
# 	"""Doc."""
# 	def nx(n):
# 	    """The n'."""
# 	    return floor((sqrt(2)-1) * n)
#
#
# 	def over2(n, x):
# 	    return n*x + n*(n+1)/2 - x*(x+1)/2
#
# 	def S(sq, n):
# 		return TailCall(over2(n, x) - S(sq, x))
#
# 	x = nx(n)
# 	return TailCallable(S(sqrt(2), n))(range(n))
#
# print recursive_beatty(5)
#
#
#
# def sieve_of_eratosthenes(max):
#     def helper(nums, known_primes):
#         if len(nums) > 0:
#             current_prime = nums[0]
#             known_primes.append(current_prime)
#             return TailCall(lambda: helper(filter(lambda x: x % current_prime != 0, nums), known_primes))
#         else:
#             return known_primes
#     return TailCallable(helper)(range(2,max + 1), [])
#
# # print(sieve_of_eratosthenes(100000))


def fact(n, r=1) :
    if n <= 1 :
        return r
    else :
        return fact(n-1, n*r)

try:
	fact(1000)
except RuntimeError:
	print '!'


class TailCaller(object):
    def __init__(self, f):
        self.f = f
    def __call__(self, *args, **kwargs):
        ret = self.f(*args, **kwargs)
        while type(ret) is TailCall:
            ret = ret.handle()
        return ret

class TailCall(object):
    def __init__(self, call, *args, **kwargs):
        self.call = call
        self.args = args
        self.kwargs = kwargs
    def handle(self):
        if type(self.call) is TailCaller:
            return self.call.f(*self.args, **self.kwargs)
        else:
            return self.call(*self.args, **self.kwargs)


# @TailCaller
# def fact(n, r=1):
#     if n <= 1:
#         return r
#     else:
#         return TailCall(fact, n-1, n*r)


def nx(n):
    """The n'."""
    return floor((sqrt(2)-1) * n)


def over2(n, x):
    return n*x + n*(n+1)/2 - x*(x+1)/2

def beatty(r):
   n = 1
   while 1:
        nr = floor(n*r)
        n += 1
        yield nr

def Sx(r, x):
	# print x
	l = []
	b = beatty(2 + sqrt(2))
	while x is not 0:
		l.append(b.next())
		x -= 1
	return sum(l)

# @TailCaller
def S(sq, n):
	# return TailCall(S, sq, n)
	x = int(nx(n))
	print x
	a = over2(n, x)
	b = Sx(sq, x)
	print a, b
	return a - b

sq = sqrt(2)
n = 10**100
# print S(sq, n)

# print fact(10000)

print ceil(sqrt(2)/2 * n**2 + n/5)
