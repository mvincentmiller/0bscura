"""Foobar Solution #5... It's a Beatty Sequence"""
from math import floor, sqrt, ceil, modf
#
#
# def nx(n):
#     """The n'."""
#     return floor((sqrt(2)-1) * n)
#
# def over2(n, x):
#     return n*x + n*(n+1)/2 - x*(x+1)/2
#
# def S(sq, n):
#     # z = [x * sqrt(2) for x in range(n + 1)]
#     x = nx(n)
#     # print x
#     print over2(n, x), answ(x)
#     return over2(n, x) - beatty_sum(sq, x)
#
# def beatty(r):
#    n = 1
#    while 1:
#         nr = floor(n*r)
#         print nr
#         n += 1
#         yield nr
#
# def beatty_sum(n, r):
#     b = beatty(sqrt(2))
#     l = [b.next() for i in range(n)]
#     return sum(l)
#
#
#
# print beatty_sum(5, sqrt(2))
# print beatty_sum(10, sqrt(2))
# def answer(str_n):
#     """Doc."""
#     n = int(str_n)
#     return str(int(S(sqrt(2), n)))
#


# def iter_cycle(iterator):
#     saved = []
#     for iterable in iterator:
#         yield iterable
#         saved.append(iterable)
#     while saved:
#         for element in saved:
#               yield element

# a = iter_cycle(iter([1,2,3]))
# print next(a)
# print next(a)
# print next(a)
# print next(a)

# print answer('5')


def n1(n):
    a = []
    """The n'."""
    for i in range(n + 1):
        a.append(floor((sqrt(2)-1) * i))
    return sum(a)

def over2(n, x):
    return n*x + n*(n+1)/2 - x*(x+1)/2

def answer(str_n):
    """Looking at the whole..."""
    n = int(str_n)
    # print n
    r = sqrt(2)/2 * n**2 + n/5
    x = n1(n)
    y = n * (n + 1) / 2
    # print x, y
    r = x + y
    return r
    # print ceil(r)
    # print round(r)
    # print floor(r)

    # return str(int(ceil(r)))
