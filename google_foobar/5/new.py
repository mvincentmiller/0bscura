# using Tail Recursion
from math import sqrt, floor, ceil
import timeit

class TailCaller(object) :
    def __init__(self, f) :
        self.f = f
    def __call__(self, *args, **kwargs) :
        ret = self.f(*args, **kwargs)
        while type(ret) is TailCall :
            ret = ret.handle()
        return ret

class TailCall(object) :
    def __init__(self, call, *args, **kwargs) :
        self.call = call
        self.args = args
        self.kwargs = kwargs
    def handle(self) :
        if type(self.call) is TailCaller :
            return self.call.f(*self.args, **self.kwargs)
        else :
            return self.call(*self.args, **self.kwargs)

def tailcall(f) :
    def _f(*args, **kwargs) :
        return TailCall(f, *args, **kwargs)
    return _f


d = []
@TailCaller
def recur(n, b = 0):
   if not n:
     print 'done'
     return d
   else:
    # ni = ni + n
    # b += floor(sqrt(2)*n)
    d.append(floor(sqrt(2)*n))
    # print n, b
    # return n + recur(n-1)
    return tailcall(recur)(n-1, b)

# r = recur(10**10)
# start = timeit.default_timer()
# print sum(r)
# stop = timeit.default_timer()
# print stop - start

previous = {0: 0, 1: 1}

a,b=1,1
i=1
while(len(str(a))<=77):
  i=i+1
  a,b=b,a+b

# print i,len(str(a)),a

# n = 77
# print round((n * (n*sqrt(2) + sqrt(2) - 1)) / 2)

# ni = floor(n*(sqrt(2) - 1))
@TailCaller
def S(n, a = sqrt(2), x=0, r=0):
    if r > 0:
        return r, x, a
    r = (n + floor(n*(sqrt(2) - 1))) * (n + floor(n*(sqrt(2) - 1)) + 1) / 2
    z = 2 + a
    x = floor(n*(sqrt(2) - 1))
    return tailcall(S)(z + a, x, r)

print S(5)
