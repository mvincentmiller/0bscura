"""Elegance..."""
from math import sqrt, floor

class TailCaller(object) :
    def __init__(self, f) :
        self.f = f
    def __call__(self, *args, **kwargs) :
        ret = self.f(*args, **kwargs)
        while type(ret) is TailCall :
            ret = ret.handle()
        return ret

class TailCall(object) :
    def __init__(self, call, *args, **kwargs) :
        self.call = call
        self.args = args
        self.kwargs = kwargs
    def handle(self) :
        if type(self.call) is TailCaller :
            return self.call.f(*self.args, **self.kwargs)
        else :
            return self.call(*self.args, **self.kwargs)

def tailcall(f) :
    def _f(*args, **kwargs) :
        return TailCall(f, *args, **kwargs)
    return _f


@TailCaller
def recur(n, ni=0):
    if n == 0:
        return ni # "Finished count %s" % count
    ni += floor(sqrt(2)*n)
    # print ni
    return tailcall(recur)(n-1, ni)



def S(n, a=sqrt(2)):
    ni = recur(n)
    return (n + ni) * (n + ni + 1) / 2 - S((2 + a, ni))

def iter(n, ni=0):
    s = []
    while n != 0:
        ni += floor((sqrt(2)-1) * n)
        n -= 1
        # print n
    return ni

def answer(n):
    try:
        return recur(n)
    except RuntimeError:
        return '!'

# def answer(n):
#     return int(round(sqrt(2) * (n*(n+1)/2) - n /2))


# print answer(5)
# print answer(77)
