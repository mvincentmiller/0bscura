import os

# *****************************
# Environment specific settings
# *****************************

# DO NOT use "DEBUG = True" in production environments
DEBUG = True

# DO NOT use Unsecure Secrets in production environments
# Generate a safe one with:
#     python -c "import os; print repr(os.urandom(24));"
SECRET_KEY = repr(os.urandom(24))

# SQLAlchemy settings
SQLALCHEMY_DATABASE_URI = os.environ["DATABASE_URL"]
# SQLALCHEMY_DATABASE_URI = 'postgres://oclecwcf:-KjaFPhM1kMXKhxtGAPjRNr6DUOgZcY5@tantor.db.elephantsql.com:5432/oclecwcf'
# SQLALCHEMY_DATABASE_URI = 'sqlite:///../app.sqlite'
SQLALCHEMY_TRACK_MODIFICATIONS = False    # Avoids a SQLAlchemy Warning

# Flask-Mail settings
# For smtp.gmail.com to work, you MUST set "Allow less secure apps" to ON in Google Accounts.
# Change it in https://myaccount.google.com/security#connectedapps (near the bottom).
MAIL_SERVER = 'smtp.gmail.com'
# MAIL_PORT = 587
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USE_TLS = False
MAIL_USERNAME = 'clutchmoves@gmail.com'
MAIL_PASSWORD = 'wrdsjjiyhhqlxeod'
MAIL_DEFAULT_SENDER = '"Vince" <m.vincent83@gmail.com>'

ADMINS = [
    '"Vince" <m.vincent83@gmail.com>',
    ]
