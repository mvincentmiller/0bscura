import argparse
import random
import time
# import math

# from pythonosc import osc_message_builder
from pythonosc import udp_client


OSC_CLIENT_IP = "54.200.142.46"
OSC_CLIENT_PORT = 5004

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default=OSC_CLIENT_IP,
        help="The ip of the OSC server")

    parser.add_argument("--port", type=int, default=OSC_CLIENT_PORT,
        help="The port the OSC server is listening on")
    args = parser.parse_args()

client = udp_client.SimpleUDPClient(args.ip, args.port)
while 1:
    x = random.randint(1, 500)
    y = random.randint(1, 500)
    addr = "/test"
    client.send_message(addr, (x, y))
    time.sleep(1)
    print(addr, x, y, args.ip)
